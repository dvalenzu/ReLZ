TOPTARGETS := all clean test
all: setup_externals
SUBDIRS = src ext/decoder ext/decoder_elias ext/em_lz77decode/ tests

$(TOPTARGETS): $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@ $(MAKECMDGOALS)

setup_externals:
	cd ext; ./setup_sdsl.sh; ./setup_fastpfor.sh; ./setup_fse.sh; cd ..;

.PHONY: $(TOPTARGETS) $(SUBDIRS)
.PHONY: setup_externals
