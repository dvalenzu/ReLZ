#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"

function validate_fasta_std
{
  INPUT_FILE=${1}
  N_CHUNKS=20  
  N_THREADS=20



  MAX_MEM_MB=1000
  # NEEDS TO BE AS LARGE AS THE INPUT FILE.
  # OTHERWISE, IT IS VERY LIKELY TO CRASH.
  DECODE_BUFFER_SIZE=200
  ## The decode buffer needs to be sligthly larger than the ref.

  REF_SIZE=60
  #REF_SIZE_MB=10

  utils_assert_file_exists ${ReLZ_BIN} 
  utils_assert_file_exists ${DECODER64_BIN} 

  COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
  DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
  rm -f ${COMPRESSED_FILE}
  rm -f ${DECOMPRESSED_FILE}

  echo "Encoding..."
  echo "Line: ${ReLZ_BIN} ${INPUT_FILE} -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -f -s" 
  ${ReLZ_BIN} ${INPUT_FILE} -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -f -s
  echo "Decoding..."
  ${DECODER64_BIN}  ${COMPRESSED_FILE} ${DECOMPRESSED_FILE} -p ${DECODE_BUFFER_SIZE}
  echo "Verifying (diff)..."

  TMP_FILE=tmp.output
  utils_fasta_to_plain ${INPUT_FILE} ${TMP_FILE}
  utils_assert_equal_files ${DECOMPRESSED_FILE} ${TMP_FILE}
}

function validate_fasta_plus
{
  INPUT_FILE=${1}
  N_CHUNKS=20  
  N_THREADS=20



  MAX_MEM_MB=1
  # NEEDS TO BE AS LARGE AS THE INPUT FILE.
  # OTHERWISE, IT IS VERY LIKELY TO CRASH.
  DECODE_BUFFER_SIZE=200
  ## The decode buffer needs to be sligthly larger than the ref.

  REF_SIZE=50
  #REF_SIZE_MB=10

  utils_assert_file_exists ${ReLZ_BIN} 
  utils_assert_file_exists ${DECODER64_BIN} 

  COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
  DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
  rm -f ${COMPRESSED_FILE}
  rm -f ${DECOMPRESSED_FILE}
  D=3
  echo "Encoding..."
  echo "Line: ${ReLZ_BIN} ${INPUT_FILE} -f -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -s -d ${D}"
  ${ReLZ_BIN} ${INPUT_FILE} -f -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -s -d ${D}
  echo "Decoding..."
  ${DECODER64_BIN}  ${COMPRESSED_FILE} ${DECOMPRESSED_FILE} -p ${DECODE_BUFFER_SIZE}
  echo "Verifying (diff)..."

  TMP_FILE=tmp.output
  utils_fasta_to_plain ${INPUT_FILE} ${TMP_FILE}
  utils_assert_equal_files ${DECOMPRESSED_FILE} ${TMP_FILE}
}

