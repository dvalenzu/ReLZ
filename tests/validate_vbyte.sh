#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"

rm -f tmp.*
#N_CHUNKS=10
N_CHUNKS=1
# ch=30 will test feature to allow chunks shorter than ref.
N_THREADS=1



MAX_MEM_MB=1

utils_assert_file_exists ${ReLZ_BIN} 
utils_assert_file_exists ${DECODER_VBYTE_BIN} 


rm -f ${DATA_DIR}/*TMP*
rm -f ${DATA_DIR}/*rlzp
for REF_SIZE in 500; do
  for D in 0  5 ; do
    for INPUT_FILE in ${DATA_DIR}/*.*; do
      COMPRESSED_FILE="tmp.relz.$RANDOM.PID${PROCID}"
      DECOMPRESSED_FILE="tmp.decomp.$RANDOM.PID${PROCID}"
      rm -f ${COMPRESSED_FILE}
      rm -f ${DECOMPRESSED_FILE}
      echo "Encoding..."
      echo "Line: ${ReLZ_BIN} ${INPUT_FILE} -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -s -d ${D} -e VBYTE"
      ${ReLZ_BIN} ${INPUT_FILE} -l ${REF_SIZE} -o ${COMPRESSED_FILE} -c ${N_CHUNKS} -t ${N_THREADS} -m ${MAX_MEM_MB} -s -d ${D} -e VBYTE
      echo "Decoding..."
      ${DECODER_VBYTE_BIN}  ${COMPRESSED_FILE} -o ${DECOMPRESSED_FILE}
      echo "Verifying (diff)..."
      cmp ${DECOMPRESSED_FILE} ${INPUT_FILE}
      rm -f tmp.*
      echo "---"
      echo "ok"
      echo "---"
    done
  done
done

utils_success_exit
