#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 
echo ":::::::::"
echo "OUTPUT::::"
echo ":::::::::"
./validate_output.sh

echo ":::::::::"
echo "TRANSFORMER::::"
echo ":::::::::"
./validate_transformer.sh
echo ":::::::::"
echo "PLAIN::::"
echo ":::::::::"
echo ""
./validate_plain.sh

echo ":::::::::"
echo "MAX::::"
echo ":::::::::"
echo ""
./validate_max.sh

echo ":::::::::"
echo "PLUS::::"
echo ":::::::::"
echo ""
./validate_plus.sh

echo ":::::::::"
echo "VBYTE::::"
echo ":::::::::"
echo ""
./validate_vbyte.sh

echo ":::::::::"
echo "ELIAS::::"
echo ":::::::::"
echo ""
./validate_elias.sh

echo ":::::::::"
echo "IGNORE::::"
echo ":::::::::"
echo ""
./validate_ignore_and_parse.sh

echo ":::::::::"
echo "FASTA::::"
echo ":::::::::"
echo ""
./validate_fasta.sh

echo ":::::::::"
echo "USING VALGRIND::::"
echo ":::::::::"
echo ""
./validate_valgrind.sh
rm -f tmp.*
