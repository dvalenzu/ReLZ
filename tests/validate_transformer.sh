#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./tests_config.sh"


#DATA_DIR="/home/dvalenzu/Data"
for INPUT_FILE in ${DATA_DIR}/vbyte/*; do
  for PTR_TYPE in "DIST" "POS" ; do
    utils_assert_file_exists ${INPUT_FILE}
    echo ""
    echo "*******"
    echo "Testing with: ${INPUT_FILE}"
    echo "*******"

    VBYTE_TO_PLAIN=../src/vbyte_to_plain
    VBYTE_TO_SPLIT=../src/vbyte_to_split
    SPLIT_TO_VBYTE=../src/split_to_vbyte

    utils_assert_file_exists ${VBYTE_TO_SPLIT} 
    utils_assert_file_exists ${SPLIT_TO_VBYTE} 

    PREFIX="TMP_PREFIX"
    RECONSTRUCTED_FILE="TMP_RECONSTRUCTED"
    rm -f TMP_*


    ${VBYTE_TO_SPLIT} ${INPUT_FILE} -o ${PREFIX}  -p ${PTR_TYPE}
    ${SPLIT_TO_VBYTE} ${PREFIX} -o ${RECONSTRUCTED_FILE} -p ${PTR_TYPE}

    ## THE RECONSTRUCTED VBYTE MIGHT BE DIFFERENT.
    ## THIS IS FINE AS WE MAY HAVE RE-WRITTEN, e.g. LONG PHRASES.
    ## THE REAL TEST IS AFTER DECOMPRESSING:
    echo "Verifying (diff)..."
    if ! cmp ${RECONSTRUCTED_FILE} ${INPUT_FILE};
    then
      echo "VBYTE FILES  DIFFER, BUT LET's TEST DECOMPRESSION:"
      DECOMPRESSED_1="TMP_expanded_1"
      DECOMPRESSED_2="TMP_expanded_2"
      ${VBYTE_TO_PLAIN} ${RECONSTRUCTED_FILE} -o TMP_PLAIN_1
      ${VBYTE_TO_PLAIN} ${INPUT_FILE} -o TMP_PLAIN_2
      ${DECODER64_BIN}  TMP_PLAIN_1  ${DECOMPRESSED_1} -b 1
      ${DECODER64_BIN}  TMP_PLAIN_2  ${DECOMPRESSED_2} -b 1
      cmp ${DECOMPRESSED_1} ${DECOMPRESSED_2};
    fi
    #rm -f TMP_*
    echo "---"
    echo "ok"
    echo "---"
  done
done
rm -f TMP_*
utils_success_exit
