#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail 

source "./utils.sh"
source "./validate_fasta_utils.sh"

FASTA_DATA_DIR="${DATA_DIR}/fasta/"

#TODO: all the tests...
#for FILE_NAME in ${FASTA_DATA_DIR}/genome1.fa; do
for FILE_NAME in ${FASTA_DATA_DIR}/*.fa; do
  validate_fasta_std ${FILE_NAME}
  validate_fasta_plus ${FILE_NAME}
done
utils_success_exit
