/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <stdint.h>
#include <getopt.h>
#include <omp.h>

#include <cstdlib>
#include <string>
#include <iostream>

#include "./iterative_parser.h"

void print_help(char **argv);
void print_help(char **argv) {
	cerr << "usage: << " << argv[0] << " INPUT_FILE " << endl;
	cerr << endl;
	cerr << "Options: " << endl;

	cerr << " -v verbosity level 0,1,.. (default 0)" << endl;
	cerr << " -d Maximum number of iterations. 0 => RLZ , 1=> RLZ + one iteration, 2,3,... (default: 5)" << endl;
	cerr << " -l Size of prefix used as reference in the first iteration, in MB (see -s)" << endl;
	cerr << " -m MAX_MEMORY_MB (default 8000). This is an approximate value, because e.g. the phrases are held in memory" << endl;
	cerr << " -s To indicate that the -l (also -I) parameter are in bytes" << endl;
	cerr << " -t N_THREADS (default: 1)" << endl;
	cerr << " -f indicated that input and reference are FASTA files" << endl;
	cerr << " -o Output name (default: INPUT_FILE.rlz" << endl;
	cerr << " -e Encodign scheme for the output. Values: PLAIN, VBYTE, ELIAS, MAX.  Default: PLAIN" << endl;
	cerr << " -p To indicate if pointers are stored as (pos,len) or (dist,len). Values: POS, DIST. Default: POS " << endl;

	cerr << endl;
	cerr << " If -l is not given, MAX_MEMORY_MB will be use to estimate the largest acceptable value in the first iteration." << endl;
	cerr << endl;
	cerr << "Advanced options:" << endl;
	cerr << " -F forces 64 bits for sa_t, regardlless the length of the reference. For testing purposes" << endl;
	cerr << " -c N_CHUNKS  (default: N_THREADS)" << endl;
}

int main(int argc, char **argv) {
	size_t reference_factor = 1024*1024;
	int is_fasta = 0;
	size_t n_threads = 1;
	size_t n_partitions = 0;
	size_t max_memory_MB = 8000;
	size_t reference_len_opt = 0;
	size_t max_depth = 5;
  // TODO: default should be VBYTE, shouldn't ?
	EncodingType final_encoding = EncodingType::PLAIN;
  PointerType pointer_type = PointerType::POS;
  int verbosity = 0;
	int c;
	string output_string;
	while ((c = getopt(argc, argv, "v:d:i:o:l:t:c:m:sfe:p:")) != -1) {
		switch(c) {
			case 'v':
				verbosity = atoi(optarg);
				break;
			case 'd':
				max_depth = (size_t)atoi(optarg);
				break;
			case 'l':
				reference_len_opt = std::stoull(string(optarg));
				break;
			case 't':
				n_threads = (size_t)atoi(optarg);
				break;
			case 'c':
				n_partitions = (size_t)atoi(optarg);
				break;
			case 'm':
				max_memory_MB = (size_t)atoi(optarg);
				break;
			case 'o':
				output_string = string(optarg);
				break;
			case 's':
				reference_factor = 1;
				break;
			case 'f':
				is_fasta = 1;
				break;
			case 'p':
        if (strcmp(optarg, "POS") == 0) {
          pointer_type = PointerType::POS;
        } else if (strcmp(optarg, "DIST") == 0) {
          pointer_type = PointerType::DIST;
        } else {
          cerr << "-p option not recognized." << endl;
          print_help(argv);
          exit(EXIT_FAILURE);
        }
				break;
			case 'e':
        if (strcmp(optarg, "PLAIN") == 0) {
          final_encoding = EncodingType::PLAIN;
        } else if (strcmp(optarg, "VBYTE") == 0) {
          final_encoding = EncodingType::VBYTE;
        } else if (strcmp(optarg, "ELIAS") == 0) {
          final_encoding = EncodingType::ELIAS;
        } else if (strcmp(optarg, "MAX") == 0) {
          final_encoding = EncodingType::MAX;
        } else {
          cerr << "-e option not recognized." << endl;
          print_help(argv);
          exit(EXIT_FAILURE);
        }
				break;
			default:
				print_help(argv);
				exit(EXIT_FAILURE);
		}
	}
	int rest = argc - optind;
	if (rest != 1) {
		print_help(argv);
		exit(EXIT_FAILURE);
	}

	string input_string = string(argv[optind]);
	if (output_string.size() == 0) {
		output_string = input_string + ".rlzp";
	}

	if (n_partitions == 0) {
		n_partitions = 3*n_threads;
	}
	if (n_partitions < n_threads) {
		n_partitions = n_threads;
	}
#ifdef _OPENMP
	omp_set_num_threads(n_threads);
#endif

	size_t reference_len_bytes = reference_factor*reference_len_opt;

	// long double t1 = Tools::wclock();
	IterativeParser MyParser(input_string,
													 output_string,
													 final_encoding,
                           pointer_type,
													 max_depth,
													 reference_len_bytes,
													 n_threads,
													 n_partitions,
													 max_memory_MB,
													 verbosity,
													 is_fasta);
	MyParser.Parse();
	// long double t2 = Tools::wclock();
	// long double final_time = t2 -t1;

	MyParser.printStats();
	// std::cout << "(external total time(s)) : " << final_time << std::endl;
	fflush(stdout);
	fflush(stderr);
	return EXIT_SUCCESS;
}
