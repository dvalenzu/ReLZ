/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_RANDOM_ACCESS_READER_H_
#define ReLZ_SRC_RANDOM_ACCESS_READER_H_

#include <string>
#include "./tools.h"

template <typename T>
class RandomAccessReader{
	public:
		RandomAccessReader() {
    }
		virtual T get(size_t pos) {
      return (T)pos;
    }
		virtual ~RandomAccessReader() {
    }
};
#endif  // ReLZ_SRC_RANDOM_ACCESS_READER_H_
