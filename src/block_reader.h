/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_BLOCK_READER_H_
#define ReLZ_SRC_BLOCK_READER_H_

#include <string>
#include "./tools.h"

template <typename text_t>
class BlockReader{
  public:
    BlockReader();
    void Init(std::string filename,
              size_t initial_offset_in_bytes,
              size_t block_len,
              int verbosity_);
    void LoadFirstBlock(text_t * buffer_ext, size_t buffer_len);
    void AsynchPreLoadNext();
    void PreLoadNext();
    void Close();
    text_t * GetNextBlock(size_t * ans_buffer_len);
    bool IsReady() const {
      return (processed_len == text_len);
    }
    ~BlockReader();

  private:
    bool cleaned_up;
    int verbosity;
    size_t text_len;
    size_t text_len_bytes;
    size_t block_len;
    size_t processed_len;
    size_t total_blocks;
    FILE * fp;
    text_t * buffer_main;
    text_t * buffer_next;
    size_t buffer_next_len;
    int b_id;
};
#endif  // ReLZ_SRC_BLOCK_READER_H_
