/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
   */
#include "./iterative_parser.h"
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <string>
#include <stdint.h>

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>

#include "./tools.h"
#include "./parse_tools.h"
#include "./rlz_parser.h"

using std::cin;
using std::cout;
using std::endl;
using std::vector;
using std::string;


IterativeParser::IterativeParser() {
}

IterativeParser::IterativeParser(string _input_filename,
                                 string _output_filename,
                                 EncodingType _final_encoding,
                                 PointerType _pointer_type,
                                 size_t _max_depth,
                                 size_t _first_reference_len,
                                 size_t _n_threads,
                                 size_t _n_partitions,
                                 size_t _max_memory_MB,
                                 int _verbosity,
                                 bool _is_fasta) {
  input_filename = _input_filename;
  output_filename = _output_filename;
  final_encoding = _final_encoding;
  pointer_type = _pointer_type;
  max_depth = _max_depth;
  first_reference_len = _first_reference_len;
  n_threads = _n_threads;
  n_partitions = _n_partitions;
  max_memory_MB = _max_memory_MB;
  verbosity = _verbosity;
  is_fasta = _is_fasta;
  n_factors = 0;
}

void IterativeParser::Parse() {
  level_reports.resize(1);
  long double t0, t1, t2;
  string RLZ_filename = output_filename+".TMP.rlz";
  RlzParameters first_round_params(input_filename.c_str(),
                                   RLZ_filename.c_str(),
                                   first_reference_len,
                                   (size_t)0,
                                   n_threads,
                                   n_partitions,
                                   max_memory_MB,
                                   1,  // bytes per symbol: text
                                   0,  // not forcing large sa.
                                   verbosity,
                                   is_fasta);
  // We restrict SA(ref) to fit in 32 bits, unless otherwise specified via compilation flag.
  // RlzParser will truncate attempts to use a reference that does not fit in 32 bits.
#ifndef LARGEREFERENCES
  if (verbosity) {
    cerr << "Using 32-bits reference for first level." << endl;
  }
  RlzParser<uint8_t, uint32_t> FirstPass(first_round_params);
#else
  if (verbosity) {
    cerr << "LARGEREFERENCES: Using 64-bits reference for first level." << endl;
  }
  RlzParser<uint8_t, uint64_t> FirstPass(first_round_params);
#endif
  if (verbosity >= 2) {
    cout << "RLZ parsing..." << endl;
  }
  t1 = Tools::wclock();
  t0 = t1;
  FirstPass.Parse();
  t2 = Tools::wclock();
  long double rlz_time = t2 -t1;
  assert(FirstPass.skipped_area_factors == 0);
  size_t ready_factors = FirstPass.lz77_factors;

  bool we_are_ready = (FirstPass.rlz_factors == 0 || max_depth == 0);
  if (we_are_ready) {
    assert(FirstPass.rlz_factors !=0 || FirstPass.total_factors == FirstPass.lz77_factors);
    n_factors = FirstPass.total_factors;
    ParseTools::EncodeFinalOutput(RLZ_filename, output_filename, final_encoding, pointer_type);
    t2 = Tools::wclock();
    total_time = t2 - t0;
    FirstPass.ReportFactors(level_reports[0]);
    FirstPass.ReportTime(level_reports[0]);
    computeStats();
    return;
  }

  if (verbosity >= 2) {
    cout << "Packing phrases..." << endl;
  }
  string PACKED_filename =  RLZ_filename + ".packed";
  t1 = Tools::wclock();
  ParseTools::VbyteToPackedIn64(RLZ_filename, PACKED_filename);
  t2 = Tools::wclock();
  long double pack_time = t2 -t1;

  if (verbosity >= 2) {
    cout << "Recursive call to next depth..." << endl;
  }
  t1 = Tools::wclock();
  string LZ77_filename = RLZ_filename + ".packed.lz77";
  RecursiveParsePhrases(PACKED_filename,
                        LZ77_filename,
                        ready_factors,
                        1);
  Tools::remove_file_or_die(PACKED_filename); 
  t2 = Tools::wclock();
  long double recursion_time = t2 -t1;

  if (verbosity >= 2) {
    cout << "ReParsing..." << endl;
  }
  t1 = Tools::wclock();
  
  string REPARSED_filename = RLZ_filename + ".reparsed";
  n_factors = ParseTools::ReParseFile(RLZ_filename, LZ77_filename, REPARSED_filename, max_memory_MB);
  
  t2 = Tools::wclock();
  long double reparse_time = t2 -t1;
  t1 = Tools::wclock();
  ParseTools::EncodeFinalOutput(REPARSED_filename, output_filename, final_encoding, pointer_type);
  t2 = Tools::wclock();
  long double encode_time = t2 -t1;
  total_time = t2 - t0;

  FirstPass.ReportFactors(level_reports[0]);
  FirstPass.ReportTime(level_reports[0]);
  level_reports[0] += "--------------------------------------\n";
  level_reports[0] += "Rlz TOTAL time 	: " + std::to_string(rlz_time) + "\n";
  level_reports[0] += "Pack      time 	: " + std::to_string(pack_time) + "\n";
  level_reports[0] += "Recursion time 	: " + std::to_string(recursion_time) + "\n";
  level_reports[0] += "ReParse   time 	: " + std::to_string(reparse_time) + "\n";
  level_reports[0] += "Encode    time 	: " + std::to_string(encode_time) + "\n";
  level_reports[0] += "--------------------------------------\n";
  level_reports[0] += "TOTAL     time 	: " + std::to_string(total_time) + "\n";
  
  computeStats();
}

void IterativeParser::RecursiveParsePhrases(string phrases_input,
                                            string phrases_output,
                                            size_t ready_factors,
                                            size_t depth) {
  assert(level_reports.size() == depth);
  level_reports.resize(depth + 1);
  long double t1, t2;

  string RLZ_filename = phrases_input + ".Depth" + std::to_string(depth)+".rlz";
  RlzParameters params(phrases_input.c_str(),
                       RLZ_filename.c_str(),
                       0,  // ref = 0, so it gets infered based on memory limit.
                       ready_factors,  // to be ignored
                       n_threads,
                       n_partitions,
                       max_memory_MB,  // memory
                       8,  // bytes per symbol
                       0,  // No-force large sa
                       verbosity,  // non-verbose
                       0);  // Not a fasta file.
  // b = 8 -> first param is 64.
  RlzParser<uint64_t, uint64_t> * Parser = new RlzParser<uint64_t, uint64_t>(params);

  if (verbosity >= 2) {
    cout << endl;
    cout << "Starting recursion depth " << depth << endl;
    cout << "RLZ Parsing..." << endl;
  }
  t1 = Tools::wclock();
  Parser->Parse();
  t2 = Tools::wclock();
  long double rlz_time = t2 -t1;

  ready_factors += Parser->lz77_factors;
  bool we_are_ready = (Parser->rlz_factors == 0 || depth == max_depth);
  if (we_are_ready) {
    Parser->ReportFactors(level_reports[depth]);
    Parser->ReportTime(level_reports[depth]);
    Tools::rename_file_or_die(RLZ_filename, phrases_output);
    delete(Parser);
    return;
  }
  if (verbosity >= 2) {
    cout << "Packing..." << endl;
  }
  string PACKED_filename =  RLZ_filename + ".packed";
  t1 = Tools::wclock();
  ParseTools::VbyteToPackedIn64(RLZ_filename, PACKED_filename);
  t2 = Tools::wclock();
  long double pack_time = t2 -t1;

  if (verbosity >= 2) {
    cout << "Recursive call..." << endl;
  }
  string LZ77_filename = RLZ_filename+ ".packed.lz77";
  t1 = Tools::wclock();
  RecursiveParsePhrases(PACKED_filename,
                        LZ77_filename,
                        ready_factors,
                        depth+1);
  Tools::remove_file_or_die(PACKED_filename); 
  t2 = Tools::wclock();
  long double recursion_time = t2 -t1;

  if (verbosity >= 2) {
    cout << "Reparsing..." << endl;
  }
  t1 = Tools::wclock();
  
  ParseTools::ReParseFile(RLZ_filename, LZ77_filename, phrases_output, max_memory_MB);
  
  t2 = Tools::wclock();
  long double reparse_time = t2 -t1;

  Parser->ReportFactors(level_reports[depth]);
  Parser->ReportTime(level_reports[depth]);
  level_reports[depth] += "--------------------------------------\n";
  level_reports[depth] += "Rlz TOTAL time 	: " + std::to_string(rlz_time) + "\n";
  level_reports[depth] += "Pack      time 	: " + std::to_string(pack_time) + "\n";
  level_reports[depth] += "Recursion time 	: " + std::to_string(recursion_time) + "\n";
  level_reports[depth] += "ReParse   time 	: " + std::to_string(reparse_time) + "\n";
  level_reports[depth] += "--------------------------------------\n";
  delete(Parser);
  return;
}

void IterativeParser::computeStats() {
  bytes_output = 0;
  if (final_encoding != EncodingType::MAX) {
    bytes_output = Tools::file_size_or_fail(output_filename.c_str());
  } else {
    for (std::string suffix : MULTI_SUFFIXES) {
      string curr_filename = output_filename + suffix;
      bytes_output += Tools::file_size_or_fail(curr_filename.c_str());
    }
  }
  
  bytes_input = Tools::file_size_or_fail(input_filename.c_str());
  compression_ratio = (long double)bytes_output/(long double)bytes_input;
  bits_per_char = 8.0*(long double)bytes_output/(long double)bytes_input;
  secs_per_MiB = total_time*1024*1024/(long double)bytes_input;
  secs_per_GiB = secs_per_MiB*1024;
  MiB_per_sec = 1.0 / secs_per_MiB;
  GiB_per_sec = 1.0 / secs_per_GiB;
}

void IterativeParser::printStats() {
  cout << "Input succesfully compressed, output written in: " << output_filename << endl;
  cout << "Parsing performed in " << level_reports.size() << " passes" << endl;
  if (verbosity >= 1) {
    for (size_t i = 0; i < level_reports.size(); i++) {
      cout << endl;
      cout << i << "-th level report: " << endl;
      cout << endl;
      cout << level_reports[i];
    }
    cout << endl;
    cout << endl;
  }
  cout << "Final Wtime(s)    : " << total_time << std::endl;
  cout << "N Phrases         : " << n_factors << std::endl;
  cout << "Compression ratio : " << compression_ratio << std::endl;
  cout << "Bits per char     : " << bits_per_char << std::endl;
  cout << "Bits output       : " << bytes_output*8 << std::endl;
  cout << "seconds per MiB   : " << secs_per_MiB << std::endl;
  cout << "seconds per GiB   : " << secs_per_GiB << std::endl;
  cout << "MiB per second    : " << MiB_per_sec << std::endl;
  cout << "GiB per second    : " << GiB_per_sec << std::endl;
}

IterativeParser::~IterativeParser() {
}


