/**
 * @file    compute_lz77.hpp
 * @section LICENCE
 *
 * This file contains implementations of the series of KKP-LZ77
 * factorization algorithms described in the paper:
 *
 *   Juha Karkkainen, Dominik Kempa, Simon J. Puglisi:
 *   Linear Time Lempel-Ziv Factorization: Simple, Fast, Small.
 *   24th Annual Symposium on Combinatorial Pattern Matching (CPM 2013).
 *
 * and its journal version:
 *
 *   Juha Karkkainen, Dominik Kempa, Simon J. Puglisi:
 *   Lazy Lempel-Ziv Factorization Algorithms.
 *   ACM Journal of Experimental Algorithmics, 2016.
 *
 * See: http://www.cs.helsinki.fi/group/pads/
 *
 * Copyright (C) 2013-2018
 *   Juha Karkkainen <juha.karkkainen (at) cs.helsinki.fi>
 *   Dominik Kempa <dominik.kempa (at) gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 **/

#ifndef __COMPUTE_LZ77_HPP_INCLUDED
#define __COMPUTE_LZ77_HPP_INCLUDED

#include <stdint.h>
#include <vector>
#include <algorithm>
#include "./parse_writer.h"


namespace compute_lz77 {
/*
template<
  typename char_type,
  typename text_offset_type>
uint64_t parse_phrase(
    uint64_t, uint64_t, uint64_t, uint64_t, const char_type *,
    std::vector<std::pair<text_offset_type, text_offset_type> > &);
*/
template<
  typename char_type,
  typename text_offset_type>
uint64_t parse_phrase(
    uint64_t, uint64_t, uint64_t, uint64_t, const char_type *,
    ParseWriter &,
    uint64_t offset);
// DV version using parse writer.
template<
  typename char_type,
  typename text_offset_type>
uint64_t kkp2n(
    const char_type * const text,
    const text_offset_type * const sa,
    const uint64_t text_length,
    ParseWriter &parse_writer,
    uint64_t offset) {
  uint64_t nphrases = 0;

  // Handle special case.
  if (text_length == 0)
    return 0;

  // Compute the PSV array.
  text_offset_type *psv = new text_offset_type[text_length];
  {
    uint64_t prev_plus = 0;
    for (uint64_t i = 0; i < text_length; ++i) {
      const uint64_t cur = (uint64_t)sa[i];
      while (prev_plus > 0 && prev_plus - 1 > cur) {
        const uint64_t j = prev_plus - 1;
        prev_plus = ((uint64_t)psv[j] == j) ?
          (uint64_t)0 : (uint64_t)psv[j] + 1;
      }
      psv[cur] = (text_offset_type)((prev_plus == 0) ? cur : prev_plus - 1);
      prev_plus = cur + 1;
    }
  }

  // Compute LZ77 phrases using the PSV values.
  // NSV values are computed on the fly from PSV.
  {
    Factor f1;
    f1.pos = text[0];
    f1.len = 0;
    parse_writer.Save(&f1, 1);
    nphrases++;
    
    uint64_t next = 1;
    uint64_t list_head = 0;
    text_offset_type *invphi = psv;
    for (uint64_t i = 1; i < text_length; ++i) {
      const uint64_t psv_pos = (uint64_t)psv[i];
      uint64_t nsv_pos = i;
      if (psv_pos == i) {
        nsv_pos = (uint64_t)invphi[list_head];
        invphi[i] = (text_offset_type)nsv_pos;
        invphi[list_head] = (text_offset_type)i;
      } else {
        if (psv_pos != list_head) {
          nsv_pos = (uint64_t)invphi[psv_pos];
          invphi[i] = invphi[psv_pos];
        } else {
          invphi[i] = invphi[list_head];
          list_head = i;
        }
        invphi[psv_pos] = (text_offset_type)i;
      }
      if (i == next) {
        next = parse_phrase<char_type, text_offset_type>(i, text_length, psv_pos, nsv_pos, text, parse_writer, offset);
        nphrases++;
      }
    }
  }

  // Clean up.
  delete[] psv;
  return nphrases;
}

template<
  typename char_type,
  typename text_offset_type>
uint64_t parse_phrase(
    const uint64_t i,
    const uint64_t text_length,
    const uint64_t psv,
    const uint64_t nsv,
    const char_type * const text,
    ParseWriter &parse_writer,
    uint64_t offset) {

  uint64_t pos = 0;
  uint64_t len = 0;

  if (nsv == i) {
    while (text[i + len] == text[psv + len])
      ++len;
    pos = psv;
  } else if (psv == i) {
    while (i + len < text_length && text[i + len] == text[nsv + len])
      ++len;
    pos = nsv;
  } else {
    while (text[psv + len] == text[nsv + len])
      ++len;
    if (text[i + len] == text[psv + len]) {
      ++len;
      while (text[i + len] == text[psv + len])
        ++len;
      pos = psv;
    } else {
      while (i + len < text_length && text[i + len] == text[nsv + len])
        ++len;
      pos = nsv;
    }
  }
  Factor ans;
  if (len == 0) {
    ans.pos = text[i];
    ans.len = (uint64_t)0;
  }
  else {
    ans.pos = pos + offset;  // TODO(FUTURE): maybe we move this to WRITER. it will also remove a similar offset parameter from dictionary.cpp who is doing the same increment.
    ans.len = len;
  }
  parse_writer.Save(&ans, 1);
  return i + std::max((uint64_t)1, len);
}

}  // namespace compute_lz77

#endif  // __COMPUTE_LZ77_HPP_INCLUDED
