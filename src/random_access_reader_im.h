/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_RANDOM_ACCESS_READER_IM_H_
#define ReLZ_SRC_RANDOM_ACCESS_READER_IM_H_

#include <string>
#include <sdsl/vectors.hpp>
#include "./parse_streamer.h"
#include "./random_access_reader.h"
#include "./tools.h"


template <typename T>
class RandomAccessReaderIM : public RandomAccessReader<T> {
	public:
		RandomAccessReaderIM();
		explicit RandomAccessReaderIM(std::string sums_filename);
		T get(size_t pos);
		~RandomAccessReaderIM();

	private:
    sdsl::enc_vector<sdsl::coder::elias_gamma> encoded_data;
};
#endif  // ReLZ_SRC_RANDOM_ACCESS_READER_IM_H_
