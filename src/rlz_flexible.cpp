/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#include <stdint.h>
#include <getopt.h>

#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <cstring>
#include <string>
#include <iostream>
#include <sstream>
#include <omp.h>

#include "tools.h"
#include "parse_tools.h"
#include "rlz_parser.h"

void print_help(char **argv);
void print_help(char **argv) {
  cerr << "usage: << " << argv[0] << "INPUT_FILE " << endl;
  cerr << endl;
  cerr << "Options: " << endl;

  cerr << " -l Size of prefix used as reference, in MB (see -s)" << endl;
  cerr << " -m MAX_MEMORY_MB (default 8000). This is an approximate value, because e.g. the phrases are held in memory" << endl;
  cerr << " -s To indicate that the -l (also -I) parameter are in bytes" << endl;
  cerr << " -t N_THREADS (default: 1)" << endl;
  cerr << " -f indicated that input and reference are FASTA files" << endl;
  cerr << " -o Output name (default: INPUT_FILE.rlz" << endl;
  cerr << endl;
  cerr << " If -l is not given, MAX_MEMORY_MB will be use to estimate the largest acceptable value." << endl;
  cerr << endl;
  cerr << "Advanved options:" << endl;
  cerr << " -I Prefix length before reference to be skipped. Those are naively parsed into literlas, to still generate a LZ77-like parsing. Not compatible with -f (FASTA)" << endl;
  cerr << " -b [1,4,8] (default 1) interpret the input as symbols of b bytes. Not compatible with -f (FASTA)" << endl;
  cerr << " -F forces 64 bits for sa_t, regardlless the length of the reference. For testing purposes" << endl;
  cerr << " -c N_CHUNKS  (default: N_THREADS)" << endl;
}

int main(int argc, char **argv) {
  bool force_large = false;
  size_t reference_factor = 1024*1024;
  int is_fasta = 0;
  size_t n_threads = 1;
  size_t n_partitions = 1;
  size_t max_memory_MB = 8000;
  size_t reference_len_opt = 0;
  size_t ignore_len_opt = 0;
  int c;
  size_t bytes_per_symbol = 1;
  string output_filename;
  while ((c = getopt (argc, argv, "I:i:o:l:b:t:c:m:sfF")) != -1) {
    switch(c) {
      case 'I':
        ignore_len_opt = (size_t)atoi(optarg);
        break;
      case 'l':
        reference_len_opt = (size_t)atoi(optarg);
        break;
      case 'b':
        bytes_per_symbol = (size_t)atoi(optarg);
        break;
      case 't':
        n_threads = (size_t)atoi(optarg);
        break;
      case 'c':
        n_partitions = (size_t)atoi(optarg);
        break;
      case 'm':
        max_memory_MB = (size_t)atoi(optarg);
        break;
      case 'o':
        output_filename = string(optarg);
        break;
      case 's':
        reference_factor = 1;
        break;
      case 'f':
        is_fasta = 1;
        break;
      case 'F':
        force_large = true;
        break;
      default:
        print_help(argv);
        exit(0);
    }
  }

  int rest = argc - optind;
  if (rest != 1 ) {
    print_help(argv);
    exit(EXIT_FAILURE);
  }
  char * input_filename = argv[optind];
  if (output_filename.size() == 0) {
    output_filename = string(input_filename);
    output_filename += ".rlzp";
  }

  if (n_partitions < n_threads) {
    n_partitions = n_threads;
  }
  // actually, this sets an upper limit on number of threads.
#ifdef _OPENMP
  omp_set_num_threads(n_threads);
#endif

  if ((reference_len_opt == 0 && max_memory_MB == 0) ||
      (is_fasta && ignore_len_opt) ||
      (is_fasta && bytes_per_symbol != 1)) {
    print_help(argv);
    exit(EXIT_FAILURE);
  }

  size_t input_len_bytes = Tools::file_size_or_fail(input_filename);
  size_t ignore_len_bytes = ignore_len_opt*reference_factor;
  size_t reference_len_bytes = reference_len_opt*reference_factor;

  if (input_len_bytes % bytes_per_symbol) {
    std::cout << "Input len is not a multiple of b" << endl;
    std::cout << "Aborting" << endl;
    exit(33);
  }
  if (reference_len_bytes % bytes_per_symbol) {
    std::cout << "Reference is given in bytes and should be a multiple of b. This was not the case. " << endl;
    std::cout << "Aborting" << endl;
    exit(33);
  }
  if (ignore_len_bytes % bytes_per_symbol) {
    std::cout << "Reference is given in bytes and should be a multiple of b. This was not the case. " << endl;
    std::cout << "Aborting" << endl;
    exit(33);
  }

  size_t reference_len = reference_len_bytes/bytes_per_symbol;
  size_t ignore_len = ignore_len_bytes/bytes_per_symbol;

  int verbosity = 99;
  string tmp_filename = output_filename + ".tmp";
  RlzParameters rlz_params(input_filename,
                           tmp_filename.c_str(),
                           reference_len,
                           ignore_len,
                           n_threads,
                           n_partitions,
                           max_memory_MB,
                           bytes_per_symbol,
                           force_large,
                           verbosity,
                           is_fasta);

  size_t n_factors;
switch (bytes_per_symbol) {
    case 1:
      {
        if (reference_len < INT32_MAX && !force_large) {
          RlzParser<uint8_t, uint32_t> MyParser(rlz_params);
          MyParser.Parse();
          ParseTools::EncodeFinalOutput(MyParser.output_filename,
                                        output_filename,
                                        EncodingType::PLAIN,
                                        PointerType::POS);
          n_factors = MyParser.total_factors;
        } else {
          cerr << "Not supportrd anymore, could be activated though." << endl;
          /*
             RlzParser<uint8_t, uint64_t> MyParser(rlz_params);
             MyParser.Parse();
             MyParser.TransformOutputToPlain();
             n_factors = MyParser.total_factors;
             */
        }
        break;
      }
      /*
         case 4:
         {
         assert(!is_fasta);
         RlzParser<uint32_t, uint64_t> MyParser(rlz_params);
         MyParser.Parse();
         MyParser.TransformOutputToPlain();
         n_factors = MyParser.total_factors;
         break;
         }
         */
    case 8:
      {
        assert(!is_fasta);
        RlzParser<uint64_t, uint64_t> MyParser(rlz_params);
        MyParser.Parse();
        ParseTools::EncodeFinalOutput(MyParser.output_filename,
                                      output_filename,
                                      EncodingType::PLAIN, 
                                      PointerType::POS);
        n_factors = MyParser.total_factors;
      }
      break;

    default:
      cerr << "Using " << bytes_per_symbol << " bytes per_symbol is not supported." << endl;
      print_help(argv);
      exit(0);
  }
  std::cout << "N Phrases: " << n_factors << std::endl;
  std::cout << std::endl;
  fflush(stdout);
  fflush(stderr);
  return EXIT_SUCCESS;
}
