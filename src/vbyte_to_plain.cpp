/* ReLZ
   Copyright (C) 2018 Daniel Valenzuela

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see http://www.gnu.org/licenses/ .
   */
#include <stdint.h>
#include <getopt.h>
#include <omp.h>

#include <cstdlib>
#include <string>
#include <iostream>

#include "./common.h"
#include "./parse_tools.h"
#include "./parse_writer.h"
#include "./parse_streamer.h"

using std::cerr;
using std::string;
using std::endl;
using std::cout;
using std::vector;

void print_help(char **argv);
void print_help(char **argv) {
  cerr << "usage: << " << argv[0] << " INPUT_FILE " << endl;
  cerr << endl;
  cerr << "Options: " << endl;

  cerr << " -o prefix for output (default: INPUT_FILE.POS and INPUT_FILE.LEN" << endl;
  cerr << " -v verbosity level 0,1,.. (default 0)" << endl;
}

int main(int argc, char **argv) {
  int verbosity = 0;
  string output_filename;
  int c;
  while ((c = getopt(argc, argv, "v:o:")) != -1) {
    switch(c) {
      case 'v':
        verbosity = atoi(optarg);
        break;
      case 'o':
        output_filename = string(optarg);
        break;
      default:
        print_help(argv);
        exit(0);
    }
  }
  int rest = argc - optind;
  if (rest != 1) {
    print_help(argv);
    exit(EXIT_FAILURE);
  }

  string input_string = string(argv[optind]);
  if (output_filename.size() == 0) {
    output_filename = input_string;
  }
  if (verbosity) 
    cout << "output prefix : " << output_filename << endl;

  
  vector<uint32_t> ptrs;
  vector<uint32_t> lens;

  ParseTools::EncodePlain(input_string, output_filename, PointerType::POS);
  fflush(stdout);
  fflush(stderr);
  return EXIT_SUCCESS;
}

