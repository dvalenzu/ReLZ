/* ReLZ
    Copyright (C) 2018 Daniel Valenzuela

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/ .
*/
#ifndef ReLZ_SRC_PARSE_TOOLS_H_
#define ReLZ_SRC_PARSE_TOOLS_H_
#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <vector>
#include "./common.h"


namespace ParseTools {
  void ComputeSumsStream(std::string parse_filename, std::string out_filename, size_t &estimated_ram_IM_MB);
  size_t ReParseFile(std::string rlz_filename, std::string lz_filename, std::string new_output_name, size_t max_memory_MB);

  std::vector<uint64_t> GetParseData64(std::string filename, PointerType pointer_type);
  void GetParse(std::string input_filename, 
                PointerType pointer_type,
                std::vector<uint64_t> &ptrs,
                std::vector<uint32_t> &lenghts);
  void EncodeFinalOutput(std::string input_filename, std::string output_filename, EncodingType final_encode, PointerType pointer_type);
  void RenameMultiOutput(std::string original_filename, std::string new_filename);
  void EncodePlain(std::string input_filename, std::string output_filename, PointerType pointer_type);
  void EncodeElias(std::string input_filename, std::string output_filename, PointerType pointer_type);
  void EncodeVbyte(std::string input_filename, std::string output_filename, PointerType pointer_type);
  void EncodeMax(std::string input_filename, std::string output_filename, PointerType pointer_type);
  FILE * open_file(const char *path, size_t *n);
  void SaveWithVbyte(std::vector<uint32_t> mydata, std::string output_filename);
  void SaveWithVbyte(uint32_t* data, size_t data_len, std::string output_filename);
  void SaveWithVbyte(std::vector<uint64_t> mydata, std::string output_filename);
  void SaveWithVbyte(uint64_t* data, size_t data_len, std::string output_filename);
  void SaveWithCodec(std::vector<uint32_t> mydata, std::string output_filename);
  void SaveWithCodec(uint32_t* data, size_t data_len, std::string output_filename);
  void SaveMixed(std::vector<uint64_t> mydata, std::string output_prefix);
  std::vector<uint32_t> DecodeVbyte(std::string filename);
  std::vector<uint32_t> DecodeWithCodec(std::string filename, uint64_t nphrases);
  uint32_t*  DecodeWithCodec2(std::string filename, uint64_t nphrases);
  //uint32_t *  DecodeWithCodec(std::string filename);
  std::vector<uint64_t> DecodeMixed(std::string filename, uint64_t nphrases);

  void FSEOutput(std::string prefix);
  void DeFSEOutput(std::string prefix);
  void VbyteToPackedIn64(std::string input_name, std::string packed_name);
}  // namespace ParseTools

#endif  // ReLZ_SRC_PARSE_TOOLS_H_
