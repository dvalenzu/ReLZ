#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>

#include "stream.h"
#include "utils.h"
#include "lzdecoder.hpp"

using std::cerr;
using std::endl;

void print_help(char **argv);
void print_help(char **argv) {
	fprintf(stderr, "%s <infile> <outfile> -p prefix_length, in MB (default: 1024) -b bytes_per_symbol (default: 1)\n\n"
					"Decodes LZ77 encoding stored in <infile>\n"
					"and writes it into <outfile>. It assumes that the sources are in the <prefix_length>  first bytes.\n", argv[0]);
}

int main(int argc, char **argv) {
	int bytes_per_symbol = 1;
	long prefix_size = 0;
	int c;
	while ((c = getopt (argc, argv, "b:p:")) != -1) {
		switch(c) {
			case 'b':
				bytes_per_symbol = atoi(optarg);
				break;
			case 'p':
  			prefix_size = (atoi(optarg) * (1L << 20));
				break;
			default:
				print_help(argv);
				exit(0);
		}
	}
  int rest = argc - optind;
  if (rest != 2 ) {
    cerr << "Incorrect number of arguments." << endl;
    print_help(argv);
    exit(0);
  }
	if (prefix_size == 0) {
		// defaulting to 7 GB:
		prefix_size =  ( 1024 * (1L << 20));
		prefix_size /= bytes_per_symbol;
	}

	const char *  input_name = argv[optind];	
	const char *  output_name = argv[optind + 1];	
	
	switch(bytes_per_symbol) {
		case 1:
			Decode<uint8_t>(input_name, output_name, prefix_size);
			break;
		case 4:
			Decode<uint32_t>(input_name, output_name, prefix_size);
			break;
		case 8:
			Decode<uint64_t>(input_name, output_name, prefix_size);
			break;
		default:
			cerr << "Using " << bytes_per_symbol << " bytes per_symbol is not supported." << endl;
			print_help(argv);
			exit(0);
	
	}

}

