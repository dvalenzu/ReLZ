#include <cstdio>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdint.h>

#include "stream.h"


template<typename text_t> 
void Decode(const char * input,
						const char * output,
						long prefix_size) {

  stream_reader<uint64_t> *reader = new stream_reader<uint64_t>(input);

  fprintf(stderr, "Input file: %s\n", input);
  fprintf(stderr, "Output file: %s\n", output);
  //fprintf(stderr, "Prexix size: %s\n", argv[3]);
  
  //long prefix_size = (atoi(argv[3]) * (1L << 20));

  text_t *prefix= new text_t[prefix_size];
  std::ofstream ofs(output);
  
  if (!ofs.good()) {
    fprintf(stderr, "Error opening %s \n.", output);
    exit(EXIT_FAILURE);
  }
  

  uint64_t ptr = 0, dbg = 0, tot = 0;
	text_t tmp;

  while (!(reader->empty())) {
    uint64_t pos = reader->read();
    uint64_t len = reader->read();

    if (!len) {
      if (ptr < prefix_size) {
        prefix[ptr++] = (text_t)pos;
      }
      //ofs.put((text_t)pos);
     	tmp = (text_t)pos;
			ofs.write((char*)(&tmp), sizeof(tmp));
    }
    else {
      for (uint64_t j = 0; j < len; ++j) {
        if (ptr < prefix_size) {
          prefix[ptr++] = prefix[pos + j];
        }
        //ofs.put(prefix[pos+j]);
     		tmp = (text_t)prefix[pos+j];
				ofs.write((char*)(&tmp), sizeof(tmp));
      }
    }

    // Print progress.
    tot += len;
    if (tot - dbg > (10 << 20)) {
      dbg = tot;
      fprintf(stderr, "Decompressed %.2LfMiB \r",
          (long double)tot / (1 << 20));
    }
  }

  if (!ofs.good()) {
    fprintf(stderr, "An error occurred with the output file.\n");
  }
  fprintf(stderr, "DONE\n");
  delete[] prefix;

}
