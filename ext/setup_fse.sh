#!/usr/bin/env bash
set -o errexit
set -o nounset

cd FiniteStateEntropy;
make
cd ..
