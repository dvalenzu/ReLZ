#!/usr/bin/env bash
set -o errexit
set -o nounset

cd FastPFor;
cmake .
make
cd ..
