# ReLZ: Lempel-Ziv-like parsing in small space

ReLZ is a general purpose data compressor that approximates LZ77 greedy parsing using (arbitrarily) small memory.
This is achieved by using an RLZ-style parser as intermediate step to reduce the input size, and later perform LZ77 greedy parsing. 
ReLZ  is specially effective for very large (i.e. larger than the available RAM) and highly repetitive collections (e.g. genomes, versioned documents), where it outperforms state-of-the-art compressors.

### Prerequisites
ReLZ requires:

* A 64-bit Linux operating system. 
* g++ version 4.9 or higher.
* cmake

ReLZ relies in the following external libraries:
* [SDSL][LIBSDSL]
* [FastPFOR][LIBFASTPFOR]
* [FiniteStateEntropy][LIBFSE]

### Installing
```
git clone https://gitlab.com/dvalenzu/ReLZ
cd ReLZ
make
```

### Usage

To compress a file run
```
./ReLZ FILENAME
```
For more details run
```
./ReLZ --help
```

### Citation

If you use the library in an academic setting please cite the
[following paper][ARXPAPER].

[ARXPAPER]: https://arxiv.org/abs/1903.01909
[LIBSDSL]: https://github.com/simongog/sdsl-lite/
[LIBFASTPFOR]: https://github.com/lemire/FastPFor
[LIBFSE]: https://github.com/Cyan4973/FiniteStateEntropy
